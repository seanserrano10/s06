//Mini-Activity Serrano, Sean Patrick D. BCS21

let faveFood = "Sushi";
let sum1 = 150 + 9;
let product1 = 100 * 90;
let isActive = true;
let faveRestaurant = ["Jollibee", "McDo", "KFC", "Chowking", "Wendy's"];
let faveArtist = {
	firstName: "Alexander James",
	lastName: "O'Connor",
	stageName: "Rex Orange County",
	birthday: "May 4",
	age: 24,
	bestAlbum: "Apricot Princess",
	bestSong: "Television / So Far So Good",
	isActive: true,
};

console.log(faveFood, sum1, product1, isActive, faveRestaurant, faveArtist);

function divide (num1, num2){
	console.log(`${num1} / ${num2} = ${num1/num2}`);
	return num1/num2;
};

let quotient = divide(50, 5);

console.log(`The result of the division is: ${quotient}`);